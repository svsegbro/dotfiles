" Sets how many lines of history VIM has to remember
set history=500

" When a file is edited, its plugin file is loaded (provided there is one for given filetype)
filetype plugin on

" When a file is edited, its indent file is loaded (provided there exists one for given filetype) 
filetype indent on

" When a file has been detected to have been changed outside of Vim 
" and it has not been changed inside of Vim, automatically read it again.
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file (see next line)
" Here we map <leader> to a comma 
let mapleader = ","

" Fast saving, using <leader>w
nmap <leader>w :w!<cr>

" :W sudo saves the file (useful for handling the permission-denied error)
" '%' means 'the current file'
" '> /dev/null' throws away standard output
command W w !sudo tee % > /dev/null

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Minimal number of screen lines to keep above and below the cursor. 
" This will make some context visible around where you are working.
set so=7

" When 'wildmenu' is on, command-line completion operates in an enhanced mode.  
" On pressing 'wildchar' (usually <Tab>) to invoke completion, the possible matches are shown just above the command line, 
" with the first match highlighted (overwriting the status line, if there is one).
set wildmenu

" A file that matches with one of these patterns is ignored when completing file or directory names.
set wildignore=*.o,*~,*.pyc,*/.git/*

" Show the line and column number of the cursor position
set ruler

" Number of screen lines to use for the command-line.  Helps avoiding |hit-enter| prompts.
set cmdheight=2

" Configure backspace like it works in most programs
set backspace=indent,eol,start

" Allow to move the the previous/next line when moving the cursor left/right 
" when the cursor is on the first/last character in the line.
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" Override the 'ignorecase' option if the search pattern contains upper case characters.
set smartcase

" Highlight search results
set hlsearch

" While typing a search command, show where the pattern, as it was typed so far, matches.  
" The matched string is highlighted. 
set incsearch 

" When a bracket is inserted, briefly jump to the matching one.  
" The jump is only done if the match can be seen on the screen. 
set showmatch 

" Tenths of a second to show the matching paren, when 'showmatch' is set.
set mat=2

" No annoying audio/visual bells on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" When non-zero, a column with the specified width is shown at the side of the window which indicates open and closed folds. 
set foldcolumn=1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable syntax highlighting
syntax enable 

" Set colorscheme
colorscheme desert

" We use a dark background, always
set background=dark

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" End-of-line (<EOL>) formats that will be tried when starting to edit a new buffer 
" and when reading a file into an existing buffer => Unix as standard file type.
set ffs=unix,dos,mac

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Use spaces instead of tabs
set expandtab
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Indentation 
set autoindent 
set smartindent

" Wrap lines
set wrap 

""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""

" Visual mode pressing * or # searches for the current selection
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Map <Space> to / (search) 
nmap <space> /

" Disable highlight when <leader><cr> is pressed
nmap <silent> <leader><cr> :noh<cr>

" Mappings for moving between windows 
nmap <C-j> <C-W>j
nmap <C-k> <C-W>k
nmap <C-h> <C-W>h
nmap <C-l> <C-W>l

" Mappings for managing tabs
nmap <leader>tn :tabnew<cr>
nmap <leader>to :tabonly<cr>
nmap <leader>tc :tabclose<cr>
nmap <leader>tm :tabmove 
nmap <leader>t<leader> :tabnext 

" Let 'tl' toggle between this and the last accessed tab
let g:lasttab = 1
nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

" Opens a new tab with the current buffer's path filled in already. 
" Super useful when editing files in the same directory
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Return to last edit position when opening files 
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Save stored code folds
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview

""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""

" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Remap 0 to first non-blank character, rather than just the first character on the line
map 0 ^

" Delete trailing white space on save, useful for some filetypes ;)
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Quickly open a buffer for scribble
nmap <leader>q :e ~/buffer<cr>

" Quickly open a markdown buffer for scribble
nmap <leader>x :e ~/buffer.md<cr>

" Toggle paste mode on and off
nmap <leader>pp :setlocal paste!<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    endif
    return ''
endfunction

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
    let l:currentBufNum = bufnr("%")
    let l:alternateBufNum = bufnr("#")

    if buflisted(l:alternateBufNum)
        buffer #
    else
        bnext
    endif

    if bufnr("%") == l:currentBufNum
        new
    endif

    if buflisted(l:currentBufNum)
        execute("bdelete! ".l:currentBufNum)
    endif
endfunction

function! CmdLine(str)
    call feedkeys(":" . a:str)
endfunction 

function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", "\\/.*'$^~[]")
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ack '" . l:pattern . "' " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

